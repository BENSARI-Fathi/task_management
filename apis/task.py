from flask.views import MethodView
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse, fields, marshal_with, inputs
from apiflask import APIBlueprint
from core.authorization import authorize
from core.models import Task
from core.schema.task import TaskOutSchema, TaskInSchema
from core.services.task import TaskService

task_bp = APIBlueprint('task', __name__)


class TaskResource(MethodView):
    @jwt_required()
    @task_bp.output(TaskOutSchema)
    def get(self, task_id):
        return TaskService.get_by_id(task_id)

    @task_bp.input(TaskInSchema(partial=True))
    @task_bp.output(TaskOutSchema)
    def put(self, task_id, json_data):
        return TaskService.update(task_id, **json_data)

    @task_bp.output({}, status_code=204)
    def delete(self, task_id):
        return TaskService.delete(task_id)


class TaskListResource(MethodView):
    @jwt_required()
    @authorize.has_role('admin')
    @task_bp.output(TaskOutSchema(many=True))
    def get(self):
        return TaskService.get_all()

    @jwt_required()
    @authorize.create(Task)
    @task_bp.input(TaskInSchema)
    @task_bp.output(TaskOutSchema)
    def post(self, json_data):
        return TaskService.add(**json_data)


class TaskListByOwner(MethodView):
    @task_bp.output(TaskOutSchema)
    def get(self, user_id):
        return TaskService.get_by_owner(user_id)


task_bp.add_url_rule('/tasks', view_func=TaskListResource.as_view("tasks"))
task_bp.add_url_rule('/tasks/<int:task_id>', view_func=TaskResource.as_view('task'))
task_bp.add_url_rule('/users/<int:user_id>/tasks', view_func=TaskListByOwner.as_view('user_tasks'))