from apiflask import APIBlueprint
from flask.views import MethodView
from core.schema.user import UserLoginSchema, UserAuthResponse
from core.services.user import UserService

login_bp = APIBlueprint('login', __name__)


class UserLogin(MethodView):

    @login_bp.input(UserLoginSchema)
    @login_bp.output(schema=UserAuthResponse, status_code=200)
    def post(self, json_data):
        token = UserService.login(json_data["email"], json_data["password"])
        print(token)
        return token


login_bp.add_url_rule('/login', view_func=UserLogin.as_view('login'))
