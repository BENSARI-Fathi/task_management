from flask.views import MethodView
from apiflask import APIBlueprint

from core.schema.user import UserAuthResponse, UserRegisterSchema
from core.services.user import UserService

register_bp = APIBlueprint('register', __name__)


class UserRegister(MethodView):
    @register_bp.input(UserRegisterSchema)
    @register_bp.output(UserAuthResponse)
    def post(self, json_data):
        print(json_data)
        token = UserService.register(**json_data)
        return token


register_bp.add_url_rule('/register', view_func=UserRegister.as_view('register'))
