from apiflask import APIBlueprint
from flask.views import MethodView
from flask_jwt_extended import jwt_required

from core.schema.user import UserOutSchema
from core.services.user import UserService

user_bp = APIBlueprint('user', __name__)


class UserDetailResource(MethodView):
    @jwt_required()
    @user_bp.output(UserOutSchema)
    def get(self, user_id: int):
        return UserService.get_by_id(user_id)


user_bp.add_url_rule('/users', view_func=UserDetailResource.as_view('users'))