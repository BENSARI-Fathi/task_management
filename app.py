from apiflask import APIFlask
from flask_migrate import Migrate

from apis.auth.user_login import login_bp
from apis.auth.user_register import  register_bp
from apis.task import task_bp
from apis.user import user_bp
from core.authorization import authorize
from core.models.base import db
from core.security import jwt

app = APIFlask(__name__)
app.config.from_object("core.config.dev.DevelopmentConfig")
db.init_app(app)
migrate = Migrate(app, db)
jwt.init_app(app)
authorize.init_app(app)

app.register_blueprint(task_bp)
app.register_blueprint(login_bp)
app.register_blueprint(register_bp)
app.register_blueprint(user_bp)


if __name__ == '__main__':
    app.run()
