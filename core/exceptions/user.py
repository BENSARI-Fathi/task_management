from werkzeug.exceptions import HTTPException


class UserNotFound(HTTPException):
    code = 400
    description = "User does not exist"


class UserAlreadyExist(HTTPException):
    code = 400
    description = "User with this email/username already exist"
