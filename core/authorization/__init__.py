from flask_authorize import Authorize
from flask_jwt_extended import get_current_user

authorize = Authorize(current_user=get_current_user)
