from apiflask import Schema
from apiflask.fields import String, Date, Integer
from apiflask.validators import OneOf


class TaskInSchema(Schema):
    title = String(required=True)
    description = String(required=True)
    due_date = Date(required=True)
    status = String(required=True, validate=OneOf(['todo', 'in_progress', 'done']))


class TaskOutSchema(Schema):
    id = Integer()
    title = String()
    description = String()
    due_date = Date()
    status = String()
