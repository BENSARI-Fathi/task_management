from apiflask import Schema
from apiflask.fields import String, Date, Integer
from apiflask.validators import Email


class UserLoginSchema(Schema):
    email = String(required=True, validate=Email())
    password = String(required=True)


class UserRegisterSchema(Schema):
    email = String(required=True, validate=Email())
    password = String(required=True)
    username = String(required=True)


class UserAuthResponse(Schema):
    access_token = String()
    refresh_token = String()


class UserOutSchema(Schema):
    id = Integer()
    username = String()
    email = String()
