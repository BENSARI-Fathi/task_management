import importlib

from flask_authorize import PermissionsMixin

from core.models.base import db


class Task(db.Model, PermissionsMixin):

    from core.models.group import Group

    __group_model__ = Group

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=True)
    due_date = db.Column(db.Date, nullable=True)
    status = db.Column(db.String(20), nullable=False, default='todo')

    def __repr__(self):
        return f'<Task {self.title}>'
