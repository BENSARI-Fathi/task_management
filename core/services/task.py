from flask_restful import abort

from core.models import Task
from core.models.base import db


class TaskService:

    @staticmethod
    def add(**payload) -> Task:
        task = Task(**payload)
        db.session.add(task)
        db.session.commit()
        return task

    @staticmethod
    def update(id: int, **payload) -> Task:
        task = Task.query.filter_by(id=id).first()
        if not task:
            abort(404, message="Task not found")
        for k, v in payload.items():
            setattr(task, k, v)
        db.session.add(task)
        db.session.commit()
        return task


    @staticmethod
    def get_all() -> list[Task]:
        return Task.query.all()

    @staticmethod
    def get_by_owner(owner_id: int) -> list[Task]:
        return Task.query.filter(Task.owner_id == owner_id).all()

    @staticmethod
    def get_by_id(task_id: int) -> Task:
        return Task.query.filter(Task.id == task_id).first()

    @staticmethod
    def delete(task_id: int):
        task = Task.query.filter(Task.id == task_id).first()
        if not task:
            abort(404, message="Task not found")
        db.session.delete(task)
        db.session.commit()
