from flask_jwt_extended import create_access_token
from sqlalchemy import or_

from core.exceptions.user import UserNotFound, UserAlreadyExist
from core.models.base import db
from core.models.user import User


class UserService:

    @staticmethod
    def login(email: str, password: str) -> "dict":
        user: "User" = User.query.filter_by(email=email).one_or_none()
        if user and user.check_password(password):
            token = create_access_token(identity=user)
            return {"access_token": token}
        raise UserNotFound()

    @staticmethod
    def register(**payload: object) -> str:
        user = User.query.filter(
            or_(User.email == payload.get("email"), User.username == payload.get('username'))
        ).first()
        if user:
            raise UserAlreadyExist()
        password = payload.pop("password")
        user = User(**payload)
        user.set_password(password)
        db.session.add(user)
        db.session.commit()
        return create_access_token(identity=user)

    @classmethod
    def get_by_id(cls, user_id):
        return User.query.get(user_id)
