import datetime


class Config(object):
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(minutes=30)
    SECRET_KEY = "h:O3-bV{>r<Uc{{Sx$rx@z|,_}Hs'f}T]O[2KrhC[)RsabdP!-B}^prv]p;7@=0"
    PROPAGATE_EXCEPTIONS = True