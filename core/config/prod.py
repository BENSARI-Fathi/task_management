from core.config.base import Config


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = "sqlite:///todo.db"
    JWT_SECRET_KEY = "cYRq9E/N+LrEAQthVmL8EKxgDnYWDvkwg8Nj/k88jGE="
    JWT_COOKIE_SECURE = True
