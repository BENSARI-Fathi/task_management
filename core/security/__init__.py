from flask import jsonify
from flask_jwt_extended import JWTManager

from core.models import User

jwt = JWTManager()


@jwt.user_identity_loader
def load_user_identity(user: "User"):
    return user.id


@jwt.user_lookup_loader
def load_user_from_jwt(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(id=identity).one_or_none()


@jwt.expired_token_loader
def expired_token_callback(jwt_header, jwt_data):
    return (
        jsonify({"message": "Token has expired", "error": "token_expired"}),
        401,
    )

@jwt.invalid_token_loader
def invalid_token_callback(error):
    return (
        jsonify(
            {"message": "Signature verification failed", "error": "invalid_token"}
        ),
        401,
    )

@jwt.unauthorized_loader
def missing_token_callback(error):
    return (
        jsonify(
            {
                "message": "Request doesnt contain valid token",
                "error": "authorization_header",
            }
        ),
        401,
    )